package se325.lab04.concert.domain;

import javax.persistence.Enumerated;

public enum Genre {
    Pop, HipHop, RhythmAndBlues, Acappella, Metal, Rock
}