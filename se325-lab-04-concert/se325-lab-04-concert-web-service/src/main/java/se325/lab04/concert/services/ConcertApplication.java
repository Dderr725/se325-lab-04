package se325.lab04.concert.services;
import se325.lab04.concert.domain.Concert;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/services")
public class ConcertApplication extends Application{
    private Set<Object> singletons = new HashSet<>();
    private Set<Class<?>> classes = new HashSet<Class<?>>();
    public ConcertApplication(){
        classes.add(ConcertResource.class);
        singletons.add(new PersistenceManager());
    }

    @Override
    public Set<Class<?>> getClasses() {
        return classes;
    }

    @Override
    public Set<Object> getSingletons()
    {
        // Return a Set containing an instance of ConcertResource that will be
        // used to process all incoming requests on Concert resources.
        return singletons;
    }
}
