package se325.lab04.concert.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import se325.lab04.concert.domain.Concert;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.awt.*;
import java.net.URI;

import static org.slf4j.LoggerFactory.getLogger;

@Path("/concerts")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
public class ConcertResource {
    private static final Logger LOGGER = getLogger(ConcertResource.class);

    @GET
    @Path("{id}")
    public Response retrieveConcert(@PathParam("id") long id){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        em.getTransaction().begin();

        Concert c = em.find(Concert.class,id);
        em.getTransaction().commit();

        if(c == null){
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        return Response.ok(c).build();
    }

    @POST
    public Response createConcert(Concert concert){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        em.getTransaction().begin();
        em.persist(concert);
        em.getTransaction().commit();

        return Response.created(URI.create("/concerts/"+ concert.getId())).build();
    }

    @DELETE
    public Response deleteAllConcerts(){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();

            TypedQuery<Concert> concertTypedQuery = em.createQuery("select c from Concert c", Concert.class);
            concertTypedQuery.executeUpdate();
            em.getTransaction().commit();

        } finally {
            em.close();
            return Response.noContent().build();
        }
    }

    @DELETE
    @Path("{id}")
    public Response deleteConcert(@PathParam("id") long id){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try {
            em.getTransaction().begin();
            Concert c = em.find(Concert.class, id);

            if (c != null) {
                em.remove(c);
            }
            em.getTransaction().commit();
        } finally {
            em.close();
            return Response.status(204).build();
        }
    }

    @PUT
    @Path("{id}")
    public Response updateConcert(Concert concert){
        EntityManager em = PersistenceManager.instance().createEntityManager();
        try{
            em.getTransaction().begin();
            Concert c = em.find(Concert.class, concert.getId());
             if(c != null){
                 em.merge(concert);
                 em.getTransaction().commit();
                 em.close();
                 return Response.status(204).build();
             }
        } finally{
            return null;
            }
    }

}
